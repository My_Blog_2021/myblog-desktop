package blog.desktop;

//import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import javax.swing.*;
import javax.swing.event.AncestorListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class UsersManageApp  extends JFrame implements ActionListener, KeyListener, MouseListener {
    private JFrame frame;
    private LoginPanel loginPanel = new LoginPanel();
    private List<User> blogUsers = new LinkedList<>();
    private JTable table;
    private JButton deleteButton;
    private JButton logoutButton;
    private JScrollPane tablePane;
    private UserTableAdapter tableModel;

    private JPanel buttonPanel;
    private Container visibleArea;


    public UsersManageApp() {

        setPreferredSize(new Dimension(600, 500));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        visibleArea = getContentPane();
        visibleArea.setLayout(new BorderLayout());
        visibleArea.add(loginPanel);

        //if loginPanel property changed, remove all from visibleArea, and then displayUserListTable
        //in the login Panel, if login succeed, change the "background" property, just to notify the login success information.
        loginPanel.addPropertyChangeListener("background", new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                if (loginPanel.isVisible() == false) {
                    //================show table ===========
                    visibleArea.removeAll();
                   visibleArea.setVisible(true);
                    //================show table ===========
                    blogUsers = createAllUsers(loginPanel);
                    displayUserTable(visibleArea, blogUsers);
                }
            }
        });

        //if visibleArea property changed, remove all from visibleArea and open loginPanel.
        //same with above, the "background" property is just edd to notify the logout success.
        visibleArea.addPropertyChangeListener("background", new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (visibleArea.isVisible() == false) {
                    visibleArea.removeAll();
                    visibleArea.setVisible(true);
                    loginPanel.setVisible(true);
                    displayLoginPanel(loginPanel);
                }
            }
        });
    }

    private void displayLoginPanel(LoginPanel loginPanel) {
        //======start to add loginPanel====================/
        frame = new JFrame("Frame Display Test");
        setTitle("myBlog - User Management System");
        frame.removeAll();
        setPreferredSize(new Dimension(600, 500));
        setLocationRelativeTo(null);
        visibleArea = getContentPane();
        visibleArea.setLayout(new BorderLayout());
        loginPanel.setVisible(true);
        LoginPanel loginPanelnew = new LoginPanel();
        visibleArea.add(loginPanelnew);
        //add observer to check if login succeed.
        loginPanelnew.addPropertyChangeListener("background", new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (loginPanelnew.isVisible() == false) {
                    visibleArea.removeAll();
                    displayUserTable(visibleArea, blogUsers);
                }
            }
        });
}

        private void displayUserTable(Container visibleArea, List<User> blogUsers) {
        //======start to display userTable====================/
        frame = new JFrame("Frame Display Test");
        setTitle("myBlog - User Management System");
        setPreferredSize(new Dimension(600, 500));
        setLocationRelativeTo(null);
        visibleArea = getContentPane();
        visibleArea.setLayout(new BorderLayout());
        // Create a table to display User details
        table = new JTable();
        tablePane = new JScrollPane(table);
        //set the table users
        tableModel = new UserTableAdapter(blogUsers);
        table.setModel(tableModel);
        visibleArea.add(tablePane);
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout());
        visibleArea.add(buttonPanel, BorderLayout.SOUTH);

        logoutButton = new JButton("Logout");
        deleteButton = new JButton("Delete");
        logoutButton.setBounds(100, 400, 100, 30);
        deleteButton.setBounds(400, 400, 100, 30);

            buttonPanel.add(logoutButton);
            buttonPanel.add(deleteButton);
            table.setCellSelectionEnabled(false);
            table.setRowSelectionAllowed(true);
            deleteButton.setEnabled(false);
            deleteButton.addActionListener(this);
            logoutButton.addActionListener(this);
            table.addMouseListener(this);
            tablePane.addMouseListener(this);
            table.addKeyListener(this);
            tablePane.addKeyListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // delete event
        if (e.getSource() == deleteButton) {
            JSONObject deleteResponse = null;
            SendRequest deleteRequest = new SendRequest();
            int statusCode = 0;

            // get user id
            int row = table.getSelectedRow();
            Object user_id = table.getValueAt(row, 0);
            String deleteUrl = "http://localhost:3000/users/delete";
            String param = "userId=" + user_id + "&" + loginPanel.getToken();

            // send delete request to delete user's info
            deleteResponse = deleteRequest.sendDeleteRequest(deleteUrl, param);
            // get status
            try {
                statusCode = (int) deleteResponse.get("status");
               // System.out.println(statusCode);
            } catch (JSONException jsonException) {
                jsonException.printStackTrace();
            }

            // delete
            if (statusCode == 204) {
                // if user deleted from database, delete the row from UI:
                blogUsers.remove(row);
                tableModel.removeRow(row, row);
                JOptionPane.showMessageDialog(null, "Successfully deleted!");

            } else if (statusCode == 401) {
                JOptionPane.showMessageDialog(null, "Failed!");
            }

            // logout event
        } else if (e.getSource() == logoutButton) {
            SendRequest logoutRequest = new SendRequest();
            int statusCode2 = 0;

            String logoutUrl = "http://localhost:3000/desktop/logout";
            String token = loginPanel.getToken();
            // send get(logout) request to logout and get status code
            try {
                statusCode2 = (int)(logoutRequest.sendGetRequest(logoutUrl, token).get("status"));
            } catch (JSONException jsonException) {
                jsonException.printStackTrace();
            }

            if (statusCode2 == 204) {
                // log out
                JOptionPane.showMessageDialog(null, "Successfully logged out!");
                //notify the logout succeed
                visibleArea.setBackground(Color.blue);
                visibleArea.setVisible(false);
                visibleArea.setBackground(Color.white);
            }
        }
    }

    private List<User> createAllUsers(LoginPanel loginPanel) {

        //create users list from JSONArray that received from login:
        JSONArray allUsersJSONArray = loginPanel.getAllUsersInfo();
        String[] strArrayRow;
        String[] strArrayFields;
        String[] titleAndValue;
        String[] values = new String[8];
        String jsonArrayStr = allUsersJSONArray.toString().replace("\"", "");
        strArrayRow = jsonArrayStr.split("\\},\\{");
        strArrayRow[0] = strArrayRow[0].substring(2, (strArrayRow[0].length()));
        strArrayRow[strArrayRow.length - 1] = strArrayRow[strArrayRow.length - 1].substring(0, (strArrayRow[strArrayRow.length - 1].length() - 2));
        for (int i = 0; i < strArrayRow.length; i++) {
            strArrayFields = strArrayRow[i].split(",");
            strArrayFields[0] = strArrayFields[0].substring(0, strArrayFields[0].indexOf("T"));
            for (int j = 0; j < strArrayFields.length; j++) {
                titleAndValue = strArrayFields[j].split(":");
                values[j] = titleAndValue[1];            }
            for (int l = 0; l < values.length; l++) {
            }

            //create users and add to blogusers list:
            blogUsers.add(new User(Integer.parseInt(values[3]), values[7], values[1], values[2], values[0], values[5], values[6]));
        }
        return blogUsers;
    }

    public void keyPressed(KeyEvent e) {}
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyReleased(KeyEvent e) {
        //if user select row by keyboard, enable delete button:
        if (table.getSelectedRow()!=-1){
            deleteButton.setEnabled(true);
        }
    }
    public void mousePressed(MouseEvent e) {}
    public void mouseClicked(MouseEvent e) {}
    @Override
    public void mouseReleased(MouseEvent e) {
        //if there's row selected, enable delete button:
        if (table.getSelectedRow()!=-1){
            deleteButton.setEnabled(true);
        }
    }
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}


    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            UsersManageApp app = new UsersManageApp();
            app.setVisible(true);
        });
    }

}

