package blog.desktop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

public class LoginPanel extends JPanel implements  ActionListener, KeyListener {

    // login UI

    private JLabel urlLabel;
    private JLabel usernameLabel;
    private JLabel passwordLabel;
    private JTextField urlTextField;
    private JTextField usernameTextField;
    private JPasswordField passwordField;
    private JButton resetButton;
    private JButton loginButton;
    private  JSONObject postResponse = null;
    private JSONObject getResponse = null;
    private  JSONArray getAllUsersInfo = null;
    private List<User> blogUsers = null;
    private String token = null;


    public LoginPanel() {
        setLayout(null);
        Font font = new Font(Font.SERIF, Font.PLAIN, 20);
        // set up label
        JLabel title = new JLabel("User Manage APP");
        urlLabel = new JLabel("URL: ");
        usernameLabel = new JLabel("Username: ");
        passwordLabel = new JLabel("Password: ");
        title.setBounds(230, 100, 200, 30);
        title.setFont(font);
        urlLabel.setBounds(150, 200, 100, 30);
        usernameLabel.setBounds(150, 250, 100,30);
        passwordLabel.setBounds(150, 300, 100, 30);

        // set up textField
        urlTextField = new JTextField("localhost:3000", 20);
        usernameTextField = new JTextField(20);
        passwordField = new JPasswordField(20);
        urlTextField.setBounds(250, 200, 200, 30);
        usernameTextField.setBounds(250, 250, 200, 30);
        passwordField.setBounds(250, 300, 200, 30);

        // set up bottom
        resetButton = new JButton("reset");
        loginButton = new JButton("login");
        resetButton.setBounds(100, 400, 100, 30);
        loginButton.setBounds(400, 400, 100, 30);

        // add action listener
        usernameTextField.addActionListener(this);
        passwordField.addActionListener(this);
        resetButton.addActionListener(this);
        loginButton.addActionListener(this);
        usernameTextField.addKeyListener(this);
        passwordField.addKeyListener(this);
        // add labels, textFields, buttons to window
        add(title);
        add(urlLabel);
        add(urlTextField);
        add(usernameLabel);
        add(usernameTextField);
        add(passwordLabel);
        add(passwordField);
        add(resetButton);
        add(loginButton);
        loginButton.setEnabled(false);

    }
    public String getToken() {return token;}
    public JSONArray  getAllUsersInfo() {
        return getAllUsersInfo;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String postUrl = "http://" + urlTextField.getText() + "/desktop/login";
        String getUrl = "http://" + urlTextField.getText() + "/users";
        String username = usernameTextField.getText();
        String password = passwordField.getText();
        String param = "username=" + username + "&password=" + password;


        int statusCode = 0;


        SendRequest loginRequest = new SendRequest();

        // loginButton event
        if (e.getSource() == loginButton) {
            // send post request to check if is an admin
            postResponse = loginRequest.sendPostRequest(postUrl, param);

            // get status and authentication token
            try {
                statusCode = (int) postResponse.get("status");
                token = "token=" + postResponse.get("token");
                //System.out.println(statusCode);
                //System.out.println(token);
            } catch (JSONException jsonException) {
                jsonException.printStackTrace();
            }

            // Successfully logged in
            if (statusCode == 204){
                // send get request
                getResponse = loginRequest.sendGetRequest(getUrl, token);

                // get status code
                try {
                    statusCode = (int) getResponse.get("status");
                } catch (JSONException jsonException) {
                    jsonException.printStackTrace();
                }

                // check if the user is an admin
                if (statusCode == 401) {
                    // is not admin
                    JOptionPane.showMessageDialog(null, "You do not have a permission");
                } else {
                    // is admin
                    try {
                        // get all users' info
                        getAllUsersInfo = (JSONArray) getResponse.get("allUsersInfo");
                    } catch (JSONException jsonException) {
                        jsonException.printStackTrace();
                    }
                    // admin panel should display
                   // login succeed
                    this.removeAll();
                    this.setSize(100,100);
                   this.setVisible(false);
                   this.setBackground(Color.white);
                }

            // Authentication failed
            } else  if (statusCode == 401){
                // Authentication failed
                JOptionPane.showMessageDialog(null, "Authentication failed!");
            }
        }

        // resetButton event
        if (e.getSource() == resetButton) {
            usernameTextField.setText("");
            passwordField.setText("");
        }

    }

    public void keyPressed(KeyEvent e) {}
    public void keyTyped(KeyEvent e) {}
    public void keyReleased(KeyEvent e) {
        String postUrl = "http://" + urlTextField.getText() + "/desktop/login";
        String username = usernameTextField.getText();
        String password = passwordField.getText();
        String param = "username=" + username + "&password=" + password;
        if (!(username.equals("")||password.equals("")))  {
        //both username and password ready
            loginButton.setEnabled(true);
        }else {
            loginButton.setEnabled(false);
        }
    }

}
