package blog.desktop;

import java.util.Objects;

public class User {
    private int userId;
    private String username;
    private String fname;
    private String lname;
    private String birthday;
    private String description;
    private String avatar;

    public User(int userId, String username, String fname, String lname, String birthday, String description, String avatar) {

        this.userId = userId;
        this.username = username;
        this.fname = fname;
        this.lname = lname;
        this.birthday = birthday;
        this.description = description;
        this.avatar = avatar;
    }


    public int getId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public String getBirthday() {
        return birthday;
    } 

    public String getDescription() {
        return description;
    }

    public String getAvatar() {
        return avatar;
    }

    
    public void setUsername(String username) {
        this.username = username;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    
    // override and implement toString()
    @Override
    public String toString(){
        return username + " (description: " + description + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User User = (User) o;
        return userId == User.userId &&
                username == User.username &&
                fname.equals(User.fname) &&
                lname.equals(User.lname) &&
                birthday.equals(User.birthday) &&
                description.equals(User.description)&&
                avatar.equals(User.avatar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, username, fname, lname, birthday, description,avatar );
    }
}
