package blog.desktop;

import javax.swing.table.AbstractTableModel;
import java.util.List;
import java.util.Optional;

public class UserTableAdapter extends AbstractTableModel {

    private List<User> users;

    public UserTableAdapter(List<User>  users) {
        this.users = users;
    }


    public void removeRow(int firstRow, int lastRow) {
        fireTableRowsDeleted(firstRow, lastRow);
    }

    @Override
    public int getRowCount() {
        return users.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        User emp = users.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return emp.getId();
            case 1:
                return emp.getUsername();
            case 2:
                return emp.getFname();
            case 3:
                return emp.getLname();
            case 4:
                return emp.getBirthday();
            case 5:
                return emp.getDescription();
            case 6:
                return emp.getAvatar();
            default:
                throw new IllegalArgumentException("Unexpected columnIndex");
        }
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "userId";
            case 1:
                return "username";
            case 2:
                return "fname";
            case 3:
                return "lname";
            case 4:
                return "birthday";
            case 5:
                return "description";
            case 6:
                return "avatar";
            default:
                throw new IllegalArgumentException("Unexpected column");
        }
    }


    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex > 0;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

        User emp = users.get(rowIndex);
        String strValue = aValue.toString();

        switch (columnIndex) {
            case 1:
                emp.setUsername(strValue);
                break;
            case 2:
                emp.setFname(strValue);
                break;
            case 3:
                emp.setLname(strValue);
                break;
            case 4:
            case 5:
                emp.setBirthday(strValue);
                break;
            case 6:
                emp.setDescription(strValue);
                break;
            case 7:
                emp.setAvatar(strValue);
                break;
        }

    }
}
