package blog.desktop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

public class SendRequest {

    /**
     * send post request to the url
     *
     * @param url the url to send request
     * @param param the param to send
     * @return the request result
     */
    public JSONObject sendPostRequest(String url, String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        JSONObject jsonObject = null;

        try {
            URL sendUrl = new URL(url);
            // open connection
            URLConnection connection = sendUrl.openConnection();

            // setup post request
            connection.setDoOutput(true);
            connection.setDoInput(true);

            // get "out" and send param
            out = new PrintWriter(connection.getOutputStream());
            out.print(param);
            out.flush();

            // get "in" and get response from the url
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = null;
            while ((line = in.readLine()) != null) {
                result = result + line;
            }
            System.out.println(result);
            jsonObject = new JSONObject(result); // parse string to jsonObj

            // catch exception
        } catch (IOException | JSONException e) {
            System.out.println("Error:" + e.getMessage());
            e.printStackTrace();
        }
        // close inputStream and outputStream
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return jsonObject;
    }

    /**
     * send get request to the url
     *
     * @param url the url to send request
     * @param param the param to send
     * @return the request result
     */
    public JSONObject sendGetRequest(String url, String param) {

        BufferedReader in = null;
        String result = "";
        JSONObject jsonObject = null;

        try {
            String urlString = url + "?" + param;
            URL sendUrl = new URL(urlString);
            // open connection
            URLConnection connection = sendUrl.openConnection();

            connection.connect();

            // get response header fields
            Map<String, List<String>> map = connection.getHeaderFields();
            for (String key: map.keySet()) {
                System.out.println(key + ":" + map.get(key));
            }

            //get "in" and get response from the url
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = null;
            while ((line = in.readLine()) != null) {
                result = result + line;
            }

            // parse to json
            jsonObject = new JSONObject(result);
            System.out.println(jsonObject);

        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }

    /**
     * send delete request to the url
     *
     * @param url the url to send request
     * @param param the param to send
     * @return the request result
     */
    public JSONObject sendDeleteRequest(String url, String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        JSONObject jsonObject = null;

        try {
            URL sendUrl = new URL(url);
            // open connection
            HttpURLConnection connection = (HttpURLConnection) sendUrl.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);

            // delete method
            connection.setRequestMethod("DELETE");

            // get "out" and send param
            out = new PrintWriter(new OutputStreamWriter(connection.getOutputStream(), StandardCharsets.UTF_8));
            out.print(param);
            out.flush();

            // get "in" and get response from the url
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = null;
            while ((line = in.readLine()) != null) {
                result = result + line;
            }
            System.out.println(result);
            jsonObject = new JSONObject(result); // parse string to jsonObj


        } catch (IOException | JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;

    }
}
